<?php
require('vendor/autoload.php');
require('helpers.php');

// Setup
$config = array(
    'host' => 'localhost',
    'database' => 'homestead',
    'username' => 'homestead',
    'password' => 'secret'
);

$dsn = sprintf('mysql:host=%s;dbname=%s', $config['host'], $config['database']);
$connection = new \Nette\Database\Connection($dsn, $config['username'], $config['password']);
$cacheStorage = new \Nette\Caching\Storages\FileStorage(__DIR__ . '/temporary');
$structure = new \Nette\Database\Structure($connection, $cacheStorage);
$database = new Nette\Database\Context($connection, $structure);

// Fetch all types of sensors
$sensors = $database->query('SELECT DISTINCT sensid FROM sensors')->fetchAll();

$dateFrom = null;
$dateTo = null;

if(isset($_GET['date_from'])) {
    $dateFrom = validateDate($_GET['date_from']);
}

if(isset($_GET['date_to'])) {
    $dateTo = validateDate($_GET['date_to']);
}

$results = array();

// This could've been done a bit better. Now we need one query for each sensor.
foreach($sensors as $sensor) {
    $sensid = $sensor['sensid'];

    $rows = $database->table('sensors')
                     ->where('sensid', $sensor['sensid']);

    if($dateFrom !== null && $dateTo !== null) {
        $rows = $rows->where('(time >= "' . $dateFrom . '" AND time <= "' . $dateTo . '")');

        echo $rows->getSql();
    }

    foreach ($rows as $row) {
        // We have these fields to play with:
        // time, sensname, sensloc, sensid, value
        if(isset($row->time) && isset($row->value)) {
            // todo: fix the calculation of the value here:
            $value = $row->value;

            $results[$sensid]['time'][] = (string)$row->time;
            $results[$sensid]['value'][] = $value;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sensors</title>

        <!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="container">
            <div class="header clearfix">
                <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active"><a href="#">Home</a></li>
                        <li role="presentation"><a href="#">About</a></li>
                        <li role="presentation"><a href="#">Contact</a></li>
                    </ul>
                </nav>
                <h3 class="text-muted">Sensors</h3>
            </div>

            <?php foreach($results as $chartid => $result): ?>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $chartid; ?>
                    <canvas
                        class="chart"
                        id="chart-<?php echo md5($chartid); ?>"
                        data-time="<?php echo htmlspecialchars(json_encode($result['time']), ENT_QUOTES, 'UTF-8'); ?>"
                        data-value="<?php echo htmlspecialchars(json_encode($result['value']), ENT_QUOTES, 'UTF-8'); ?>"
                        height="500" style="width: 100%; height: 500px;"></canvas>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/chart.min.js"></script>

        <script>
            $(function() {
                // Grab all charts and generate cool stuff
                $('canvas.chart').each(function(i, chartElem) {
                    var context = chartElem.getContext('2d');
                    var chartElem = $(chartElem);

                    var time = chartElem.data('time');
                    var values = chartElem.data('value');

                    var data = {
                        labels: time,
                        datasets: [
                            {
                                label: "Values",
                                fillColor: "rgba(151,187,205,0.2)",
                                strokeColor: "rgba(151,187,205,1)",
                                pointColor: "rgba(151,187,205,1)",
                                pointStrokeColor: "#fff",
                                pointHighlightFill: "#fff",
                                pointHighlightStroke: "rgba(151,187,205,1)",
                                data: values
                            }
                        ]
                    };

                    new Chart(context).Line(data);
                });
            });
        </script>
    </body>
</html>