<?php
function validateDate($date) {
    $d = DateTime::createFromFormat('Y-m-d', $date);

    if($d && $d->format('Y-m-d') == $date) {
        return $d->format('Y-m-d');
    }

    return null;
}